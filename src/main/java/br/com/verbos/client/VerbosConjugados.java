package br.com.verbos.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.regex.Pattern;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

public class VerbosConjugados {
	private static final String LINK = "http://localhost:8080/verbosws/rest/verbos/palavra/";
//	private static final String LINK = "http://itinerariosp.com.br:8080/verbosws/rest/verbos/palavra/";
	private static HttpURLConnection conn;
	private static int count = 1;
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		try {
			System.out.println("INICIO!");
			Mongo mongo = new Mongo("192.168.99.100", 32769);
			DB db = mongo.getDB("verbos");
			DBCollection collection = db.getCollection("lista");
			DBCollection collectionConj = db.getCollection("conjugados");
			
			BasicDBObject allQuery = new BasicDBObject();
			BasicDBObject fields = new BasicDBObject();
			fields.put("nome", 1);
			fields.put("id_verbos", 1);
			
			DBCursor cursor = collection.find(allQuery, fields);
			
			while (cursor.hasNext()) {
				BasicDBObject obj = (BasicDBObject) cursor.next();
				System.out.println("Verbo: " + unAccent(obj.getString("nome")));
				setVerbosMongoDB(getVerbosConjugados(LINK + unAccent(obj.getString("nome"))), Integer.parseInt(obj.getString("id_verbos")), collectionConj);
			}
			System.out.println("FIM!");
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
	}
	
	public static BufferedReader getVerbosConjugados(String linkVerbo) throws IOException{
		try {
			URL url = new URL(linkVerbo);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			return br;
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
		return null;
	}
	
	public static void setVerbosMongoDB(BufferedReader br, int idVerbo, DBCollection collection) throws IOException {
		try {
			String[] list = null;
			String output;
			while ((output = br.readLine()) != null) {
				int tamanho = output.replace("[", "").replace("]", "").replace("},{", "} _ {").split(" _ ").length;
				list = new String[tamanho];
				list = output.replace("[", "").replace("]", "").replace("},{", "} _ {").split(" _ ");
			}
			for (String string : list) {
				if (string != null && !string.isEmpty()){
					DBObject dbObject = (DBObject) JSON.parse(string);
					dbObject.put("id_verbos", idVerbo);
					collection.insert(dbObject);
				}
			}
			conn.disconnect();
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
		}
	}
	
	public static String unAccent(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("");
	}
	
}
