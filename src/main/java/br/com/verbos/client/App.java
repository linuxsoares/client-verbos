package br.com.verbos.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

/**
 * Hello world!
 *
 */
public class App{
    public static void main( String[] args ) {
    	
//    	String[] alfabeto = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "w", "y", "x", "z"}; 

    	try {
//    		int count = 1;
//    		String doc = new String();
    		Mongo mongo = new Mongo("192.168.99.100", 32769);
    		DB db = mongo.getDB("verbos");
    		DBCollection collection = db.getCollection("lista");
    		String[] list = null;
    		
    		BasicDBObject document = null;
    		
    		URL url = new URL("http://localhost:8080/verbosws/rest/verbos/lista");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("Accept", "application/json");

    		if (conn.getResponseCode() != 200) {
    			throw new RuntimeException("Failed : HTTP error code : "
    					+ conn.getResponseCode());
    		}

    		BufferedReader br = new BufferedReader(new InputStreamReader(
    			(conn.getInputStream())));

    		String output;
    		while ((output = br.readLine()) != null) {
    			list = new String[output.replace("[", "").replace("]", "").replace("},{", "} {").split(" ").length];
    			if (output.replace("[", "").replace("]", "").replace("},{", "}{") != null)
    				list = output.replace("[", "").replace("]", "").replace("},{", "} {").split(" ");
    		}
    		for (String string : list) {
    			System.out.println("DOC: " + string);
    			DBObject dbObject = (DBObject) JSON.parse(string);
    			collection.insert(dbObject);
			}

    		conn.disconnect();
    		System.out.println("OK");

    	  } catch (MalformedURLException e) {

    		e.printStackTrace();

    	  } catch (IOException e) {

    		e.printStackTrace();

    	  }
    }
}
