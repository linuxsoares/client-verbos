package br.com.verbos.client;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.regex.Pattern;

public class Teste {
	public static void main(String[] args) throws UnsupportedEncodingException {
		String value = "ç á";
		System.out.println(unAccent(value));
		// output : e a i _ @
	}

	public static String unAccent(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("");
	}
}
